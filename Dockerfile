FROM reg.cs.sun.ac.za/computer-science/docker-prebuild/alpine-tools

RUN echo -e "\
http://alpine.mirror.ac.za/v3.8/main\n\
http://alpine.mirror.ac.za/v3.8/community" > /etc/apk/repositories

RUN apk --no-cache add py2-pip python-dev make && pip install sphinx
